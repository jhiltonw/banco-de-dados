package Inicio;








import Brinquedos.Brinquedo;
import Pessoa.Clientes;
import Eletronicos.Celulares;
import Eletronicos.Computador;
import Eletronicos.Domestico;
import Eletronicos.Som;
import Eletronicos.Televisoes;
import Materiais.Material;
import Moveis.Moveis;
import Pessoa.Funcionario;
import javax.swing.ImageIcon;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author luanaehilton
 */

public class IniciaAdm extends javax.swing.JFrame {
 

 
    /**
     * Creates new form TelaFixa
     */
    public IniciaAdm() {
        initComponents();
        ImageIcon icone = new ImageIcon("src/imagens/logo.png");
        setIconImage(icone.getImage());
        
     
        this.setLocationRelativeTo(null);
        
       
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFrame1 = new javax.swing.JFrame();
        jPanel1 = new javax.swing.JPanel();
        computadores = new javax.swing.JButton();
        domestico = new javax.swing.JButton();
        sair = new javax.swing.JButton();
        clientes = new javax.swing.JButton();
        brinquedos = new javax.swing.JButton();
        moveis = new javax.swing.JButton();
        celulares = new javax.swing.JButton();
        logo = new javax.swing.JLabel();
        tv = new javax.swing.JButton();
        material = new javax.swing.JButton();
        som = new javax.swing.JButton();
        funcionario = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Inicio");
        setFocusCycleRoot(false);
        setResizable(false);

        jPanel1.setLayout(null);

        computadores.setText("Computadores");
        computadores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                computadoresActionPerformed(evt);
            }
        });
        jPanel1.add(computadores);
        computadores.setBounds(660, 60, 120, 23);

        domestico.setText("Eletrodomésticos");
        domestico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                domesticoActionPerformed(evt);
            }
        });
        jPanel1.add(domestico);
        domestico.setBounds(420, 60, 130, 23);

        sair.setText("Sair");
        sair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sairActionPerformed(evt);
            }
        });
        jPanel1.add(sair);
        sair.setBounds(480, 160, 51, 23);

        clientes.setText("Clientes");
        clientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clientesActionPerformed(evt);
            }
        });
        jPanel1.add(clientes);
        clientes.setBounds(100, 10, 80, 23);

        brinquedos.setText("Brinquedos");
        brinquedos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                brinquedosActionPerformed(evt);
            }
        });
        jPanel1.add(brinquedos);
        brinquedos.setBounds(290, 10, 100, 23);

        moveis.setText("Móveis");
        moveis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveisActionPerformed(evt);
            }
        });
        jPanel1.add(moveis);
        moveis.setBounds(200, 10, 70, 23);

        celulares.setText("Celulares");
        celulares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                celularesActionPerformed(evt);
            }
        });
        jPanel1.add(celulares);
        celulares.setBounds(440, 10, 90, 23);
        jPanel1.add(logo);
        logo.setBounds(0, 0, 0, 0);

        tv.setText("Televisões");
        tv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tvActionPerformed(evt);
            }
        });
        jPanel1.add(tv);
        tv.setBounds(570, 10, 90, 23);

        material.setText("Materiais de Construção");
        material.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                materialActionPerformed(evt);
            }
        });
        jPanel1.add(material);
        material.setBounds(680, 10, 180, 23);

        som.setText("Aparelhos de som");
        som.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                somActionPerformed(evt);
            }
        });
        jPanel1.add(som);
        som.setBounds(230, 60, 130, 23);

        funcionario.setText("Funcionários");
        funcionario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                funcionarioActionPerformed(evt);
            }
        });
        jPanel1.add(funcionario);
        funcionario.setBounds(360, 110, 110, 23);

        jButton1.setText("Vendas");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(540, 110, 80, 23);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/logo2.png"))); // NOI18N
        jLabel2.setPreferredSize(new java.awt.Dimension(1024, 550));
        jPanel1.add(jLabel2);
        jLabel2.setBounds(0, -20, 1030, 650);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1024, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void somActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_somActionPerformed
        // TODO add your handling code here:
        Som sons = new Som();
        sons.setVisible(true);
        dispose();
    }//GEN-LAST:event_somActionPerformed

    private void clientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clientesActionPerformed
        // TODO add your handling code here:
        Clientes cli = new Clientes();
        cli.setVisible(true);
        dispose();
    }//GEN-LAST:event_clientesActionPerformed

    private void moveisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moveisActionPerformed
        // TODO add your handling code here:
        Moveis moveis = new Moveis();
        moveis.setVisible(true);
        dispose();
    }//GEN-LAST:event_moveisActionPerformed

    private void brinquedosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_brinquedosActionPerformed
        // TODO add your handling code here:
        Brinquedo bri = new Brinquedo();
        bri.setVisible(true);
        dispose();
    }//GEN-LAST:event_brinquedosActionPerformed

    private void celularesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_celularesActionPerformed
        // TODO add your handling code here:
        Celulares cell = new Celulares();
        cell.setVisible(true);
        dispose();
    }//GEN-LAST:event_celularesActionPerformed

    private void domesticoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_domesticoActionPerformed
        // TODO add your handling code here:
        Domestico dom = new Domestico();
        dom.setVisible(true);
        dispose();
    }//GEN-LAST:event_domesticoActionPerformed

    private void tvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tvActionPerformed
        // TODO add your handling code here:
        Televisoes tv = new Televisoes();
        tv.setVisible(true);
        dispose();
    }//GEN-LAST:event_tvActionPerformed

    private void materialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_materialActionPerformed
        // TODO add your handling code here:
        Material material = new Material();
        material.setVisible(true);
        dispose();
    }//GEN-LAST:event_materialActionPerformed

    private void computadoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_computadoresActionPerformed
        // TODO add your handling code here:
        Computador pc = new Computador();
        pc.setVisible(true);
        dispose();
    }//GEN-LAST:event_computadoresActionPerformed

    private void sairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sairActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_sairActionPerformed

    private void funcionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_funcionarioActionPerformed
        // TODO add your handling code here:
        Funcionario func = new Funcionario();
        func.setVisible(true);
        dispose();
    }//GEN-LAST:event_funcionarioActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        Venda venda = new Venda();
        venda.setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public void inicia() {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(IniciaAdm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(IniciaAdm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(IniciaAdm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IniciaAdm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new IniciaAdm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton brinquedos;
    private javax.swing.JButton celulares;
    private javax.swing.JButton clientes;
    private javax.swing.JButton computadores;
    private javax.swing.JButton domestico;
    private javax.swing.JButton funcionario;
    private javax.swing.JButton jButton1;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel logo;
    private javax.swing.JButton material;
    private javax.swing.JButton moveis;
    private javax.swing.JButton sair;
    private javax.swing.JButton som;
    private javax.swing.JButton tv;
    // End of variables declaration//GEN-END:variables
}
