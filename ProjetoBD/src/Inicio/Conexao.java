package Inicio;





import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;




public class Conexao {
   
         Connection conexao;
         public Conexao(){
             
             try{
                   Class.forName("com.mysql.jdbc.Driver");
                   conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/his?useSSL=false", "root", "1234");
                   
             } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,""+e.getMessage(),"Ocorreu um erro na conexão.",JOptionPane.ERROR_MESSAGE);       
             } catch (ClassNotFoundException ex) {
                 Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
             }  
             
         }
         
         
           public int executar(String sql){
                    
            //url de conexao de bando de driver (método jdbc)
      
                      //passar comando de adicionar excluir e atualizar
                  try{
                      Statement stm = conexao.createStatement();
                      int res = stm.executeUpdate(sql);
                  
                      
                    JOptionPane.showMessageDialog(null,"Operação realizada com sucesso!");
             
                    return res;
                    
                      
                  }catch (Exception e){
                      JOptionPane.showMessageDialog(null,""+e.getMessage(),"ERRO",JOptionPane.ERROR_MESSAGE);
                      
                  }return 0;
                 
        }
          
           public ResultSet buscar(String sql){
                       
       
     
                  //consultar o banco     
               try{
                   Statement stm = conexao.createStatement();
                   ResultSet rs = stm.executeQuery(sql);
                  
                   return rs;
                   
               }catch(Exception e){
                  JOptionPane.showMessageDialog(null,""+e.getMessage(),"ERRO",JOptionPane.ERROR_MESSAGE);
                   
               }return null;
           } 
}
    

