/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Inicio.Conexao;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Hilton
 */
public class DAO_Clientes {
    Conexao conexao = new Conexao();
    
    public void Cadastrar(String nome, String cpf, String endereco, String sexo, String telefone, String email, String nascimento){
          if (nome.equals("") || cpf.equals("") || endereco.equals("") || sexo.equals("") || telefone.equals("") || email.equals("") || nascimento.equals("")) {
            JOptionPane.showMessageDialog(null, "Preencha os dados para adiconar um cliente.");
        } else {
         conexao.executar("INSERT INTO cliente(nome, cpf, endereco, sexo, telefone, email, nascimento) VALUES ('"+nome+"','"+cpf+"','"+endereco+"','"+sexo+"','"+telefone+"','"+email+"','"+nascimento+"');");
         
        }
    }

    public void Atualizar(String id, String nome, String cpf, String endereco, String sexo, String telefone, String email, String nascimento){
         if (id.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para atualizar.");
        }else if(id.equals("") || nome.equals("") || cpf.equals("") || endereco.equals("") || sexo.equals("") || telefone.equals("") || email.equals("") || nascimento.equals("")){
            JOptionPane.showMessageDialog(null, "Preencha todos os dados para atualizar.");
        }else {
        conexao.executar("UPDATE cliente set nome ='"+nome+"', cpf ='"+cpf+"', endereco='"+endereco+"', sexo='"+sexo+"', telefone='"+telefone+"', email='"+email+"', nascimento='"+nascimento+"' WHERE id ='"+id+"';");
        }
    }
    
    public void Apagar(String id){
    
        if (id.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para apagar.");
        } else {
        int Resp = JOptionPane.showConfirmDialog(null, "Deseja realmente apagar?", "Confirmação", JOptionPane.YES_NO_OPTION);
        if (Resp == JOptionPane.YES_OPTION) {
        conexao.executar("DELETE FROM cliente WHERE id='"+id+"';");
        
        }
        }
} 
    
    public ResultSet Buscar(String buscar){
        ResultSet rs = conexao.buscar("SELECT * from cliente WHERE nome like '"+buscar+"%'order by nome Asc;");
        return rs;
    }
}