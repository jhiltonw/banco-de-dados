/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;



import Inicio.Conexao;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Hilton
 */
public class DAO_Funcionario {
    Conexao conexao = new Conexao();

   
    public void Cadastrar(String nome, String cpf, String endereco, String telefone, String email, String senha, String setor){
        if (nome.equals("") || cpf.equals("") || endereco.equals("") || telefone.equals("(  )    -    ") || email.equals("") || senha.equals("") || setor.equals("")) {
         
            JOptionPane.showMessageDialog(null, "Preencha todos os dados para adiconar um cliente.");
        
        } else if(setor.equals("Administrador") || setor.equals("Estoque")){
               conexao.executar("INSERT INTO funcionario(nome, cpf, endereco, telefone, email) VALUES "
                               + "('"+nome+"','"+cpf+"','"+endereco+"','"+telefone+"','"+email+"'); "
                      
                               + "INSERT INTO administrativo(id_adm, senha, setor)values"
                               + "((select max(id) from funcionario),'"+senha+"','"+setor+"');");
                     
            }else{
             conexao.executar("INSERT INTO funcionario(nome, cpf, endereco, telefone, email) VALUES "
                               + "('"+nome+"','"+cpf+"','"+endereco+"','"+telefone+"','"+email+"'); "
                      
                               + "INSERT INTO vendedor(id_vendedor, senha, setorvenda)values"
                               + "((select max(id) from funcionario),'"+senha+"','"+setor+"');");
                     
            
        
        }
    }
    
    
    public void Atualizar(String id, String nome, String cpf, String endereco, String telefone, String email, String senha, String setor){
        if (id.equals("") ) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para atualizar.");
            
        } else if(nome.equals("") || cpf.equals("") || endereco.equals("") || telefone.equals("") || email.equals("") || senha.equals("") || setor.equals("")){
            
            JOptionPane.showMessageDialog(null, "Preencha todos os dados para atualizar.");
            
        }else {
            
            if(setor.equals("Administrador") || setor.equals("Estoque")){
            
                conexao.executar("UPDATE funcionario set nome ='"+nome+"', cpf ='"+cpf+"', endereco='"+endereco+"', telefone='"+telefone+"', email='"+telefone+"' WHERE id ='"+id+"';"
                               + "UPDATE administrativo set senha='"+senha+"', setor='"+setor+"' WHERE id_adm ='"+id+"';");
          
            }else{
                conexao.executar("UPDATE funcionario set nome ='"+nome+"', cpf ='"+cpf+"', endereco='"+endereco+"', telefone='"+telefone+"', email='"+telefone+"' WHERE id ='"+id+"';"
                               + "UPDATE vendedor set senha='"+senha+"', setorvenda='"+setor+"' WHERE id_vendedor='"+id+"';");
           
            
        }
        }
    }
    
    public void Apagar(String id){
          if (id.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para apagar.");
        } else {
            int Resp = JOptionPane.showConfirmDialog(null, "Deseja realmente apagar?", "Confirmação", JOptionPane.YES_NO_OPTION);
            if (Resp == JOptionPane.YES_OPTION) {
                conexao.executar("DELETE FROM funcionario WHERE id='"+id+"';");
                
            
            }
    }
}
    
    public ResultSet Buscar(String tabela, String buscar){
        
            if(tabela.equals("Todos")){
               
                ResultSet rs = conexao.buscar("SELECT id, nome, cpf, endereco, telefone, email from funcionario WHERE nome like '"+buscar+"%' order by nome Asc;");
                return rs;
            }
            else if(tabela.equals("Vendedor")){
            ResultSet rs = conexao.buscar("SELECT id, nome, cpf, endereco, telefone, email, setorvenda from funcionario join vendedor on id=id_vendedor WHERE nome like '"+buscar+"%'order by nome asc;");
            return rs;
            }else{
                ResultSet rs = conexao.buscar("SELECT id, nome, cpf, endereco, telefone, email, setor from funcionario join administrativo on id=id_adm  WHERE nome like '"+buscar+"%' order by nome asc;");
                return rs;
            }
            
            
    }
    

}

