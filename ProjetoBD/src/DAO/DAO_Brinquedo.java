/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Inicio.Conexao;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Hilton
 */
public class DAO_Brinquedo {
    Conexao conexao = new Conexao();
    
    public void Cadastrar(String nome, String marca, String caracteristicas, String classificacao, String valor, String quantidade){
        
        if (nome.equals("") || marca.equals("") || caracteristicas.equals("") || classificacao.equals("") || valor.equals("") || quantidade.equals("")) {
            JOptionPane.showMessageDialog(null, "Preencha os dados para adiconar um briquedo.");
        } else {
        conexao.executar("INSERT INTO produto(nome, caracteristicas, valor, quantidade) VALUES ('"+nome+"','"+caracteristicas+"','"+valor+"', '"+quantidade+"');"
                       + "INSERT INTO brinquedo(codigo, marca, classificacao)VALUES((select max(codigo) from produto), '"+marca+"', '"+classificacao+"');");
       
        }
    }
    
    public void Atualizar(String codigo, String nome, String marca, String caracteristicas, String classificacao, String valor, String quantidade){
        
        if (codigo.equals("") || nome.equals("") || marca.equals("") || caracteristicas.equals("") || classificacao.equals("") || valor.equals("") || quantidade.equals("")) {
          JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para atualizar.");
        } else {
        conexao.executar("UPDATE produto set nome ='"+nome+"', caracteristicas='"+caracteristicas+"', valor='"+valor+"', quantidade='"+quantidade+"' WHERE codigo ='"+codigo+"';"
                       + "UPDATE brinquedo set marca='"+marca+"', classificacao='"+classificacao+"' WHERE codigo='"+codigo+"';");
        }
        
    }
    
    public void Apagar(String codigo){
        
        if (codigo.equals("") ){
        JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para apagar.");
        } else {
        int Resp = JOptionPane.showConfirmDialog(null, "Deseja realmente apagar?", "Confirmacao", JOptionPane.YES_NO_OPTION);
        
        if (Resp == JOptionPane.YES_OPTION) {
        conexao.executar("DELETE FROM produto WHERE codigo='"+codigo+"'");
             }
        }
    }
    
    public ResultSet Busca(String buscar){
        
        ResultSet rs = conexao.buscar("SELECT produto.codigo, nome, marca, caracteristicas, classificacao, valor, quantidade"
                + " from produto join brinquedo on produto.codigo = brinquedo.codigo WHERE nome like '"+buscar+"%' order by nome Asc;");
        
        return rs;
    }
}
