/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Inicio.Conexao;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Hilton
 */
public class DAO_Domestico {
    Conexao conexao = new Conexao();
    
    public void Cadastrar(String nome, String marca, String modelo, String potencia, String caracteristicas, String valor, String quantidade){
        if (nome.equals("")) {
            JOptionPane.showMessageDialog(null, "Preencha os dados para adicionar um eletrodoméstico.");
        } else {
        conexao.executar("INSERT INTO produto(nome, caracteristicas, valor, quantidade) VALUES"
                    + " ('"+nome+"','"+caracteristicas+"','"+valor+"','"+quantidade+"');"
                    + "INSERT INTO domestico(codigo, marca, modelo, potencia)values"
                    + "((select max(codigo) from produto),'"+marca+"','"+modelo+"','"+potencia+"');");
        
        }
    }
    
    public void Atualizar(String codigo, String nome, String marca, String modelo, String potencia, String caracteristicas, String valor, String quantidade){
        if (codigo.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para atualizar.");
        } else {
        conexao.executar("UPDATE produto set nome= '"+nome+"',caracteristicas='"+caracteristicas+"', valor='"+valor+"', quantidade='"+quantidade+"' WHERE codigo ='"+codigo+"';"
                        + "UPDATE domestico set marca='"+marca+"', modelo ='"+modelo+"', potencia ='"+potencia+"' WHERE codigo ='"+codigo+"';");
        
        }
    }
    
    public void Apagar(String codigo){
        if (codigo.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para excluir.");
        } else {
        int Resp = JOptionPane.showConfirmDialog(null, "Deseja realmente excluir?", "Confirmacao", JOptionPane.YES_NO_OPTION);
        
        if (Resp == JOptionPane.YES_OPTION) {
        conexao.executar("DELETE FROM produto WHERE codigo='"+codigo+"'");      
          }
        }
    }
    
    public ResultSet Buscar(String buscar){
        ResultSet rs = conexao.buscar("SELECT produto.codigo, nome, marca, modelo, potencia, caracteristicas, valor, quantidade from produto join domestico on produto.codigo = domestico.codigo WHERE modelo like '"+buscar+"%' order by nome Asc;");
        
        return rs;
    }
    
}
