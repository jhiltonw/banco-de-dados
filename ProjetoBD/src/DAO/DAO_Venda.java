/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Inicio.Conexao;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Hilton
 */
public class DAO_Venda {
    Conexao conexao = new Conexao();
    
    public ResultSet BuscarProduto(String buscar, String setor){
    
        if(setor.equals("Brinquedo")){
           ResultSet rs = conexao.buscar("SELECT produto.codigo, nome, marca, caracteristicas, classificacao, valor, quantidade"
                + " from produto join brinquedo on produto.codigo = brinquedo.codigo WHERE nome like '"+buscar+"%' order by nome Asc;");
        
        return rs; 
        
        }else if(setor.equals("Celular")){
            
            ResultSet rs = conexao.buscar("SELECT produto.codigo, nome, marca, modelo, sistema, tela, armazenamento, ram, camera, processador, caracteristicas, valor, quantidade "
                                  + "from produto join celular on produto.codigo = celular.codigo WHERE modelo like '"+buscar+"%' order by nome Asc;");
        
        return rs;
        
        }else if(setor.equals("Computador")){
            
            ResultSet rs = conexao.buscar("SELECT produto.codigo, nome, marca, modelo, sistema, tela, armazenamento, ram, processador,"
                         + " caracteristicas, valor, quantidade from produto join computador on produto.codigo = computador.codigo WHERE modelo like '"+buscar+"%' order by nome Asc;");
        
        return rs;
        
        }else if(setor.equals("Domestico")){
            ResultSet rs = conexao.buscar("SELECT produto.codigo, nome, marca, modelo, potencia, caracteristicas, valor, quantidade from produto join domestico on produto.codigo = domestico.codigo WHERE modelo like '"+buscar+"%' order by nome Asc;");
        
            return rs;
            
        }else if(setor.equals("Material")){
            
             ResultSet rs = conexao.buscar("SELECT produto.codigo, nome, marca, caracteristicas, tamanho_peso, valor, quantidade from produto join material on produto.codigo = material.codigo"
                + " WHERE nome like '"+buscar+"%' order by nome Asc;");
            
             return rs;
            
        }else if(setor.equals("Moveis")){
            ResultSet rs = conexao.buscar("select produto.codigo, nome, modelo, cor, caracteristicas, valor, quantidade from produto join moveis on produto.codigo=moveis.codigo where modelo like '"+buscar+"%' order by nome asc;");
            
            return rs;
            
        }else if(setor.equals("Som")){
            
         ResultSet rs = conexao.buscar("SELECT produto.codigo, nome, marca, modelo, potencia, caracteristicas, valor, quantidade from produto join som on produto.codigo = som.codigo WHERE modelo like '"+buscar+"%' order by nome Asc;");
         
         return rs;
         
        }else if(setor.equals("Televisao")){
            
            ResultSet rs = conexao.buscar("SELECT produto.codigo, nome, marca, modelo, tela, caracteristicas, valor, quantidade from produto join televisoes on produto.codigo = televisoes.codigo WHERE modelo like '"+buscar+"%' order by nome Asc;");
            
            return rs;
            
        }
        
        
        return null;
    }
    
    public void Venda(String codigo, int quant, String quantidade){
        System.out.println(codigo+"     "+quant+"    "+quantidade);
        int x = Integer.parseInt(quantidade);
        if (codigo.equals("")) {
            
            JOptionPane.showMessageDialog(null, "Nenhum produto selecionado para vender.");
            
        } else if(x> quant){
            JOptionPane.showMessageDialog(null, "Não a produtos suficiente.");
        }
        else if(quant > 0){
            
            int Resp = JOptionPane.showConfirmDialog(null, "Deseja realmente vender este produto?", "Confirmacao", JOptionPane.YES_NO_OPTION);
        
            if (Resp == JOptionPane.YES_OPTION) {
                conexao.executar("UPDATE produto set quantidade = quantidade - '"+quantidade+"' WHERE codigo = '"+codigo+"';");
         }
        }
        
        
        
    
    }

   
    
}
