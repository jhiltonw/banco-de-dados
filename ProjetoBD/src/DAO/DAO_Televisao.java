/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Inicio.Conexao;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Hilton
 */
public class DAO_Televisao {
    Conexao conexao = new Conexao();
    
    public void Cadastrar(String nome, String marca, String modelo, String tela, String caracteristicas, String valor, String quantidade){
        if (nome.equals("") || marca.equals("") || modelo.equals("") || tela.equals("") || caracteristicas.equals("") || valor.equals("")) {
            JOptionPane.showMessageDialog(null, "Preencha os dados para adicionar uma televisão.");
        } else {
        conexao.executar("INSERT INTO produto(nome,caracteristicas, valor, quantidade) VALUES "
                     + "('"+nome+"','"+caracteristicas+"','"+valor+"','"+quantidade+"');"
                     + "INSERT INTO televisoes(codigo, marca, modelo, tela)values"
                     + "((select max(codigo) from produto), '"+marca+"', '"+modelo+"','"+tela+"');");
        
        }
    }
    
    public void Atualizar(String codigo, String nome, String marca, String modelo, String tela, String caracteristicas, String valor, String quantidade){
        if (codigo.equals("") || nome.equals("") || marca.equals("") || modelo.equals("") || tela.equals("") || caracteristicas.equals("") || valor.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para atualizar.");
        } else {
        conexao.executar("UPDATE produto set nome='"+nome+"',caracteristicas='"+caracteristicas+"', valor='"+valor+"', quantidade='"+quantidade+"' WHERE codigo ='"+codigo+"';"
                         + "UPDATE televisoes set marca='"+marca+"', modelo ='"+modelo+"', tela ='"+tela+"' WHERE codigo ='"+codigo+"';");
       
        }
    }
    
    public void Apagar(String codigo){
        if (codigo.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para apagar.");
        } else {
        int Resp = JOptionPane.showConfirmDialog(null, "Deseja realmente apagar?", "Confirmacao", JOptionPane.YES_NO_OPTION);
        
        if (Resp == JOptionPane.YES_OPTION) {
        conexao.executar("DELETE FROM produto WHERE codigo='"+codigo+"'");
        }
        }
    }
    
    public ResultSet Buscar(String busca){
            ResultSet rs = conexao.buscar("SELECT produto.codigo, nome, marca, modelo, tela, caracteristicas, valor, quantidade from produto join televisoes on produto.codigo = televisoes.codigo WHERE modelo like '"+busca+"%' order by nome Asc;");
            
            return rs;
    }
}
