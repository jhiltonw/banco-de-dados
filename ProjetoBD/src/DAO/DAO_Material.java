/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Inicio.Conexao;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Hilton
 */
public class DAO_Material {
    Conexao conexao = new Conexao();
    
    public void Cadastrar(String nome, String marca, String caracteristicas, String tamanho_peso, String valor, String quantidade){
        
        if (nome.equals("") || marca.equals("") || caracteristicas.equals("") || tamanho_peso.equals("") || valor.equals("")) {
            JOptionPane.showMessageDialog(null, "Preencha os dados para adicionar materiais.");
        } else {
        conexao.executar("INSERT INTO produto(nome, caracteristicas, valor, quantidade) VALUES"
                    + " ('"+nome+"','"+caracteristicas+"','"+valor+"','"+quantidade+"');"
                            + "INSERT INTO material(codigo, marca, tamanho_peso)values"
                            + "((select max(codigo) from produto), '"+marca+"','"+tamanho_peso+"');");
        }
    }
    
    public void Atualizar(String codigo, String nome, String marca, String caracteristicas, String tamanho_peso, String valor, String quantidade){
        
        if (codigo.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para atualizar.");
        }else if(nome.equals("") || marca.equals("") || caracteristicas.equals("") || valor.equals("")){ 
            JOptionPane.showMessageDialog(null, "Preencha todos os dados para atualizar.");
        }else {
        conexao.executar("UPDATE produto set nome ='"+nome+"', caracteristicas='"+caracteristicas+"',"
                             + "valor='"+valor+"', quantidade='"+quantidade+"' WHERE codigo ='"+codigo+"';"
                                     + "UPDATE material set marca ='"+marca+"', tamanho_peso= '"+tamanho_peso+"' where codigo='"+codigo+"';");
        }
    }
    
    public void Apagar(String codigo){
        if (codigo.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para apagar.");
        }else {
        int Resp = JOptionPane.showConfirmDialog(null, "Deseja realmente apagar?", "Confirmacao", JOptionPane.YES_NO_OPTION);
        
        if (Resp == JOptionPane.YES_OPTION) {
        conexao.executar("DELETE FROM produto WHERE codigo='"+codigo+"';");
        
        }
        }
}
    
    public ResultSet Buscar(String buscar){
        ResultSet rs = conexao.buscar("SELECT produto.codigo, nome, marca, caracteristicas, tamanho_peso, valor, quantidade from produto join material on produto.codigo = material.codigo"
                + " WHERE nome like '"+buscar+"%' order by nome Asc;");
        return rs;
    }

}
