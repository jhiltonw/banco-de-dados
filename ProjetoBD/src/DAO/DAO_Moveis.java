/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Inicio.Conexao;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Hilton
 */
public class DAO_Moveis {
    Conexao conexao = new Conexao();
    
    public void Cadastrar(String nome, String modelo, String cor, String caracteristicas, String valor, String quantidade){
        if (nome.equals("")|| modelo.equals("") || cor.equals("") || caracteristicas.equals("") || valor.equals("")) {
            JOptionPane.showMessageDialog(null, "Preencha os dadso para adicionar moveis..");
        } else {
        conexao.executar("INSERT INTO produto (nome, caracteristicas, valor, quantidade) VALUES"
                     + "('"+nome+"','"+caracteristicas+"','"+valor+"','"+quantidade+"');"
                        + "INSERT INTO moveis(codigo, modelo, cor) values"
                             + "((select max(codigo) from produto), '"+modelo+"','"+cor+"')");
        
        }
    }
    
    public void Atualizar(String codigo, String nome, String modelo, String cor, String caracteristicas, String valor, String quantidade){
        if (codigo.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para atualizar.");
        }else if(nome.equals("") || modelo.equals("") || cor.equals("") || caracteristicas.equals("") || valor.equals("")){
            JOptionPane.showMessageDialog(null, "Preencha todos os dados para atualizar.");
        }else {
        conexao.executar("UPDATE produto set nome ='"+nome+"', caracteristicas='"+caracteristicas+"', valor='"+valor+"', quantidade='"+quantidade+"' WHERE codigo ='"+codigo+"';"
                        +"UPDATE moveis set modelo ='"+modelo+"', cor='"+cor+"' WHERE codigo ='"+codigo+"';");
        }
    }

    public void Apagar(String codigo){
        if (codigo.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para apagar.");
        } else {
        int Resp = JOptionPane.showConfirmDialog(null, "Deseja realmente apagar?", "Confirmacao", JOptionPane.YES_NO_OPTION);
        
        if (Resp == JOptionPane.YES_OPTION) {
        conexao.executar("DELETE FROM produto WHERE codigo='"+codigo+"';");

        }
        }
    }
    
    public ResultSet Busca(String busca){
        System.out.println(busca);
        ResultSet rs = conexao.buscar("select produto.codigo, nome, modelo, cor, caracteristicas, valor, quantidade from produto join moveis on produto.codigo=moveis.codigo where modelo like '"+busca+"%' order by nome asc;");
        return rs;
    }
}
