/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Inicio.Conexao;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Hilton
 */
public class DAO_Celulares {
    Conexao conexao = new Conexao();
    public void Cadastrar(String nome, String marca, String modelo, String sistema, String tela, String armazenamento, String ram, String camera, String processador, String caracteristicas, String valor, String quantidade){
        
        if (nome.equals("") || marca.equals("") || modelo.equals("") || sistema.equals("") || tela.equals("") || armazenamento.equals("") || ram.equals("") || camera.equals("") || processador.equals("") || caracteristicas.equals("") || valor.equals("") || quantidade.equals("")) {
            JOptionPane.showMessageDialog(null, "Preencha os dados para adiconar um celular.");
        } else {
        conexao.executar("INSERT INTO produto(nome, caracteristicas, valor, quantidade) VALUES"
                    + " ('"+nome+"', '"+caracteristicas+"','"+valor+"','"+quantidade+"');"
                    +    "INSERT INTO celular(codigo, marca, modelo, sistema, tela, armazenamento, ram, camera, processador)VALUES"
                    + "((select max(codigo) from produto), '"+marca+"','"+modelo+"','"+sistema+"','"+tela+"','"+armazenamento+"','"+ram+"','"+camera+"','"+processador+"');");
        }
    }
    
    public void Atualizar(String codigo, String nome, String marca, String modelo, String sistema, String tela, String armazenamento, String ram, String camera, String processador, String caracteristicas, String valor, String quantidade){
    
        if (codigo.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para atualizar.");
        } else {
        conexao.executar("UPDATE produto set nome='"+nome+"',caracteristicas='"+caracteristicas+"', valor='"+valor+"', quantidade='"+quantidade+"' WHERE codigo ='"+codigo+"';"
                       + "UPDATE celular set marca='"+marca+"', modelo ='"+modelo+"', sistema='"+sistema+"',tela ='"+tela+"',armazenamento= '"+armazenamento+"',ram='"+ram+"',camera='"+camera+"',processador='"+processador+"' WHERE codigo ='"+codigo+"';");
        }
        
    }
    
    public void Apagar(String codigo){
        
        if (codigo.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para apagar.");
        } else {
       int Resp = JOptionPane.showConfirmDialog(null, "Deseja realmente apagar?", "Confirmacao", JOptionPane.YES_NO_OPTION);
        
        if (Resp == JOptionPane.YES_OPTION) {
        conexao.executar("DELETE FROM produto WHERE codigo='"+codigo+"'");
          }
        }
    }
    
    public ResultSet Busca(String buscar){
        ResultSet rs = conexao.buscar("SELECT produto.codigo, nome, marca, modelo, sistema, tela, armazenamento, ram, camera, processador, caracteristicas, valor, quantidade "
                                  + "from produto join celular on produto.codigo = celular.codigo WHERE modelo like '"+buscar+"%' order by nome Asc;");
        
        return rs;
    }

}
