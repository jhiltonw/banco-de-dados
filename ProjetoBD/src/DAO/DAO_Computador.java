/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Inicio.Conexao;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Hilton
 */
public class DAO_Computador {
    Conexao conexao = new Conexao();
    
    public void Cadastrar(String nome, String marca, String modelo, String sistema, String tela, String armazenamento, String ram, String processador, String caracteristicas, String valor, String quantidade){
        
        if (nome.equals("") || marca.equals("") || modelo.equals("") || sistema.equals("") || tela.equals("") || armazenamento.equals("") || ram.equals("") || processador.equals("") || valor.equals("")) {
            JOptionPane.showMessageDialog(null, "Preencha os dados para adiconar um computador.");
        } else {
        conexao.executar("INSERT INTO produto(nome, caracteristicas, valor, quantidade) VALUES"
                    + " ('"+nome+"','"+caracteristicas+"','"+valor+"','"+quantidade+"');"
                    +   "INSERT INTO computador(codigo, marca, modelo, sistema, tela, armazenamento, ram, processador) VALUES"
                    + "((select max(codigo) from produto), '"+marca+"','"+modelo+"','"+sistema+"','"+tela+"','"+armazenamento+"','"+ram+"','"+processador+"');");
        }
    }
    
    public void Atualizar(String codigo, String nome, String marca, String modelo, String sistema, String tela, String armazenamento, String ram, String processador, String caracteristicas, String valor, String quantidade){
        
        if (codigo.equals("") || nome.equals("") || marca.equals("") || modelo.equals("") || sistema.equals("") || tela.equals("") || armazenamento.equals("") || ram.equals("") || processador.equals("") || valor.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para atualizar.");
        } else {
        conexao.executar("UPDATE produto set nome='"+nome+"',caracteristicas='"+caracteristicas+"', valor='"+valor+"', quantidade='"+quantidade+"' WHERE codigo ='"+codigo+"';"
                       + "UPDATE computador set marca='"+marca+"', modelo ='"+modelo+"', sistema='"+sistema+"',tela ='"+tela+"',armazenamento= '"+armazenamento+"',ram='"+ram+"',processador='"+processador+"' WHERE codigo ='"+codigo+"';");
        }
    }
    
    public void Apagar(String codigo){
        
        if (codigo.equals("")) {
            JOptionPane.showMessageDialog(null, "Nenhum cadastro selecionado para apagar.");
        } else {
        int Resp = JOptionPane.showConfirmDialog(null, "Deseja realmente apagar?", "Confirmacao", JOptionPane.YES_NO_OPTION);
        
        if (Resp == JOptionPane.YES_OPTION) {
        conexao.executar("DELETE FROM produto WHERE codigo='"+codigo+"'");       
          }
        }
    }
    
    public ResultSet Busca(String buscar){
        
        ResultSet rs = conexao.buscar("SELECT produto.codigo, nome, marca, modelo, sistema, tela, armazenamento, ram, processador,"
                         + " caracteristicas, valor, quantidade from produto join computador on produto.codigo = computador.codigo WHERE modelo like '"+buscar+"%' order by nome Asc;");
        
        return rs;
    }
    
}
